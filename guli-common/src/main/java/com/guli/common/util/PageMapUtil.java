package com.guli.common.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @author jiangli
 * @since 2019/12/28 15:33
 * 前端通用分页结果map对象
 */
public class PageMapUtil {

	public static <T> Map<String,Object> getPageMap(Page<T> pageParam) {
		List<T> records = pageParam.getRecords();
		long current = pageParam.getCurrent();
		long pages = pageParam.getPages();
		long size = pageParam.getSize();
		long total = pageParam.getTotal();
		boolean hasNext = pageParam.hasNext();
		boolean hasPrevious = pageParam.hasPrevious();

		Map<String, Object> map = new HashMap<>();
		map.put("items", records);
		map.put("current", current);
		map.put("pages", pages);
		map.put("size", size);
		map.put("total", total);
		map.put("hasNext", hasNext);
		map.put("hasPrevious", hasPrevious);

		return map;
	}
}
