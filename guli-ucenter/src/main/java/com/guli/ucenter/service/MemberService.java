package com.guli.ucenter.service;

import com.guli.ucenter.entity.Member;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 会员表 服务类
 * </p>
 *
 * @author jiangli
 * @since 2019-12-12
 */
public interface MemberService extends IService<Member> {

	Integer countRegisterByDay(String day);

	/**
	 * 根据openid返回用户信息
	 */
	Member getByOpenid(String openid);

	/**
	 * 构造第三方使用网站应用授权登录的url
	 */
	String getQRCodeUrl(String state);

	/**
	 * 微信扫码登录回调
	 */
	Member callback(String code, String state);

}
