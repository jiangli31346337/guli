package com.guli.ucenter.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.guli.common.constants.ResultCodeEnum;
import com.guli.common.exception.RRException;
import com.guli.ucenter.entity.Member;
import com.guli.ucenter.mapper.MemberMapper;
import com.guli.ucenter.service.MemberService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.guli.ucenter.util.ConstantPropertiesUtil;
import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 会员表 服务实现类
 * </p>
 *
 * @author jiangli
 * @since 2019-12-12
 */
@Service
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {
    @Autowired
    private WxMpService wxMpService;
    @Autowired
    private MemberService memberService;

    @Override
    public Integer countRegisterByDay(String day) {
        return baseMapper.selectRegisterCount(day);
    }

    @Override
    public Member getByOpenid(String openid) {
        QueryWrapper<Member> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("openid", openid);
        return baseMapper.selectOne(queryWrapper);
    }

    @Override
    public String getQRCodeUrl(String state) {
        return wxMpService.buildQrConnectUrl(ConstantPropertiesUtil.WX_OPEN_REDIRECT_URL, WxConsts.QrConnectScope.SNSAPI_LOGIN, state);
    }

    @Override
    public Member callback(String code,String state) {
        try {
            WxMpOAuth2AccessToken accessToken =wxMpService.oauth2getAccessToken(code);
            String openid = accessToken.getOpenId();
            // 根据openid返回用户信息。
            Member member = memberService.getByOpenid(openid);
            if (member == null) { //新用户
                // 获得用户基本信息
                WxMpUser wxMpUser = wxMpService.oauth2getUserInfo(accessToken, null);
                //保存用户信息
                member = new Member();
                member.setNickname(wxMpUser.getNickname());
                member.setOpenid(openid);
                member.setAvatar(wxMpUser.getHeadImgUrl());
                member.setState(state);
                memberService.save(member);
            } else {
                // 更新
                member.setState(state);
                member.setGmtModified(new Date());
                memberService.updateById(member);
            }
            return member;
        } catch (WxErrorException e) {
            throw new RRException(ResultCodeEnum.FETCH_USERINFO_ERROR);
        }
    }
}
