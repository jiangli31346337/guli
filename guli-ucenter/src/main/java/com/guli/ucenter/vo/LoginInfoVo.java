package com.guli.ucenter.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户基本信息
 */
@Data
public class LoginInfoVo implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 会员id
	 */
	private String id;

	/**
	 * 昵称
	 */
	private String nickname;

	/**
	 * 用户头像
	 */
	private String avatar;
}