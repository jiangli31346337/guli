package com.guli.ucenter.config;

import lombok.Data;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class WxOpenConfig {

	@Value("${wx.open.app_id}")
	private String appId;

	@Value("${wx.open.app_secret}")
	private String appSecret;

	@Bean
	public WxMpService wxMpService() {
		WxMpService service = new WxMpServiceImpl();
		WxMpDefaultConfigImpl wxMpConfigStorage = new WxMpDefaultConfigImpl();
		wxMpConfigStorage.setAppId(appId);
		wxMpConfigStorage.setSecret(appSecret);
		service.setWxMpConfigStorage(wxMpConfigStorage);
		return service;
	}

}