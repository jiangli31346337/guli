package com.guli.ucenter.mapper;

import com.guli.ucenter.entity.Member;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * 会员表 Mapper 接口
 * </p>
 *
 * @author jiangli
 * @since 2019-12-12
 */
public interface MemberMapper extends BaseMapper<Member> {

    @Select("SELECT COUNT(1)  FROM ucenter_member WHERE DATE(gmt_create) = #{day}")
    Integer selectRegisterCount(String day);
}
