package com.guli.edu.query;

import lombok.Data;

import java.io.Serializable;

/**
 * 课程查询对象封装
 */
@Data
public class CourseQuery implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 课程名称
	 */
	private String title;

	/**
	 * 讲师id
	 */
	private String teacherId;

	/**
	 * 一级类别id
	 */
	private String subjectParentId;

	/**
	 * 二级类别id
	 */
	private String subjectId;

}