package com.guli.edu.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guli.common.vo.R;
import com.guli.edu.entity.Course;
import com.guli.edu.entity.Teacher;
import com.guli.edu.service.CourseService;
import com.guli.edu.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 讲师 前端控制器
 * </p>
 *
 * @author jiangli
 * @since 2019-11-28
 */
@RestController
@RequestMapping("/edu/teacher")
public class TeacherController {
	@Autowired
	private TeacherService teacherService;
	@Autowired
	private CourseService courseService;

	/**
	 * 讲师列表
	 */
	@GetMapping
	public R list(){

		List<Teacher> list = teacherService.list(null);
		return  R.ok().data("items", list);
	}

	/**
	 * 根据ID查询讲师
	 */
	@GetMapping(value = "{teacherId}")
	public R getById(@PathVariable String teacherId){

		//查询讲师信息
		Teacher teacher = teacherService.getById(teacherId);
		//根据讲师id查询这个讲师的课程列表
		List<Course> courseList = courseService.selectByTeacherId(teacherId);
		return R.ok().data("teacher", teacher).data("courseList", courseList);
	}

	/**
	 * 分页讲师列表
	 */
	@GetMapping(value = "{page}/{limit}")
	public R pageList(@PathVariable Long page, @PathVariable Long limit){

		Page<Teacher> pageParam = new Page<>(page, limit);
		Map<String, Object> map = teacherService.pageListWeb(pageParam);
		return  R.ok().data(map);
	}
}