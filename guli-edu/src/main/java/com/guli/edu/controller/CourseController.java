package com.guli.edu.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guli.common.vo.R;
import com.guli.edu.entity.Course;
import com.guli.edu.service.ChapterService;
import com.guli.edu.service.CourseService;
import com.guli.edu.vo.ChapterVo;
import com.guli.edu.vo.CourseWebVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 课程 前端控制器
 * </p>
 *
 * @author jiangli
 * @since 2019-11-28
 */
@RestController
@RequestMapping("/edu/course")
public class CourseController {
	@Autowired
	private CourseService courseService;
	@Autowired
	private ChapterService chapterService;

	/**
	 * 分页课程列表
	 */
	@GetMapping(value = "{page}/{limit}")
	public R pageList(@PathVariable Long page, @PathVariable Long limit) {

		Page<Course> pageParam = new Page<>(page, limit);
		Map<String, Object> map = courseService.pageListWeb(pageParam);
		return R.ok().data(map);
	}

	/**
	 * 根据ID查询课程
	 */
	@GetMapping(value = "{courseId}")
	public R getById(@PathVariable String courseId){

		//查询课程信息和讲师信息
		CourseWebVo courseWebVo = courseService.selectCourseWebVoById(courseId);
		//查询当前课程的章节信息
		List<ChapterVo> chapterVoList = chapterService.nestedList(courseId);
		return R.ok().data("course", courseWebVo).data("chapterVoList", chapterVoList);
	}

}

