package com.guli.edu.controller.admin;

import com.guli.common.vo.R;
import com.guli.edu.entity.Chapter;
import com.guli.edu.service.ChapterService;
import com.guli.edu.vo.ChapterVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/edu/chapter")
public class ChapterAdminController {

    @Autowired
    private ChapterService chapterService;

    /**
     * 新增章节
     */
    @PostMapping
    public R save(@RequestBody Chapter chapter){

        chapterService.save(chapter);
        return R.ok();
    }

    /**
     * 根据ID查询章节
     */
    @GetMapping("{id}")
    public R getById(@PathVariable String id){

        Chapter chapter = chapterService.getById(id);
        return R.ok().data("item", chapter);
    }

    /**
     * 根据ID修改章节
     */
    @PutMapping("{id}")
    public R updateById( @PathVariable String id,@RequestBody Chapter chapter){

        chapter.setId(id);
        chapterService.updateById(chapter);
        return R.ok();
    }

    /**
     * 根据ID删除章节
     */
    @DeleteMapping("{id}")
    public R removeById(@PathVariable String id){

        chapterService.removeChapterById(id);
        return R.ok();
    }

    /**
     * 嵌套章节数据列表
     */
    @GetMapping("nested-list/{courseId}")
    public R nestedListByCourseId(@PathVariable String courseId){

        List<ChapterVo> chapterVoList = chapterService.nestedList(courseId);
        return R.ok().data("items", chapterVoList);
    }
}