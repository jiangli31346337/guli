package com.guli.edu.controller.admin;

import com.guli.common.vo.R;
import com.guli.edu.dto.VideoInfoForm;
import com.guli.edu.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 课时管理
 */
@RestController
@RequestMapping("/admin/edu/video")
public class VideoAdminController {

	@Autowired
	private VideoService videoService;

	/**
	 * 新增课时
	 */
	@PostMapping("save-video-info")
	public R save(@RequestBody VideoInfoForm videoInfoForm){

		videoService.saveVideoInfo(videoInfoForm);
		return R.ok();
	}

	/**
	 * 根据ID查询课时
	 */
	@GetMapping("video-info/{id}")
	public R getVideInfoById(@PathVariable String id){

		VideoInfoForm videoInfoForm = videoService.getVideoInfoFormById(id);
		return R.ok().data("item", videoInfoForm);
	}

	/**
	 * 更新课时
	 */
	@PutMapping("update-video-info/{id}")
	public R updateCourseInfoById(@RequestBody VideoInfoForm videoInfoForm,@PathVariable String id){

		videoService.updateVideoInfoById(videoInfoForm);
		return R.ok();
	}

	/**
	 * 根据ID删除课时
	 */
	@DeleteMapping("{id}")
	public R removeById(@PathVariable String id){

		videoService.removeVideoById(id);
		return R.ok();
	}
}