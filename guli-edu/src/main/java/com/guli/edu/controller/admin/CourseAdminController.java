package com.guli.edu.controller.admin;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guli.common.vo.R;
import com.guli.edu.dto.CourseInfoForm;
import com.guli.edu.entity.Course;
import com.guli.edu.query.CourseQuery;
import com.guli.edu.service.CourseService;
import com.guli.edu.vo.CoursePublishVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author jl
 * @since 2019-12-16 15:42
 */
@RestController
@RequestMapping("/admin/edu/course")
public class CourseAdminController {
    @Autowired
    private CourseService courseService;

    /**
     * 根据ID查询课程
     */
    @GetMapping("course-info/{id}")
    public R getCourseInfoFormById(@PathVariable String id){

        CourseInfoForm courseInfoForm = courseService.getCourseInfoFormById(id);
        return R.ok().data("item", courseInfoForm);
    }

    /**
     * 新增课程
     */
    @PostMapping("save-course-info")
    public R saveCourseInfo(@RequestBody CourseInfoForm courseInfoForm){

        String courseId = courseService.saveCourseInfo(courseInfoForm);
        if(!StrUtil.isEmpty(courseId)){
            return R.ok().data("courseId", courseId);
        }else{
            return R.error().message("保存失败");
        }
    }

    /**
     * 更新课程
     */
    @PutMapping("update-course-info/{id}")
    public R updateCourseInfoById(@RequestBody CourseInfoForm courseInfoForm, @PathVariable("id") String id){

        courseService.updateCourseInfoById(courseInfoForm);
        return R.ok();
    }

    /**
     * 分页课程列表
     */
    @GetMapping("{page}/{limit}")
    public R pageQuery(@PathVariable Long page, @PathVariable Long limit, CourseQuery courseQuery){

        Page<Course> pageParam = new Page<>(page, limit);

        courseService.pageQuery(pageParam, courseQuery);
        List<Course> records = pageParam.getRecords();

        long total = pageParam.getTotal();

        return  R.ok().data("total", total).data("rows", records);
    }

    /**
     * 根据ID删除课程
     */
    @DeleteMapping("{id}")
    public R removeById(@PathVariable String id){
        courseService.removeCourseById(id);
        return R.ok();
    }

    /**
     * 根据ID获取课程发布信息
     */
    @GetMapping("course-publish-info/{id}")
    public R getCoursePublishVoById(@PathVariable String id){

        CoursePublishVo courseInfoForm = courseService.getCoursePublishVoById(id);
        return R.ok().data("item", courseInfoForm);
    }

    /**
     * 根据id发布课程
     */
    @PutMapping("publish-course/{id}")
    public R publishCourseById(@PathVariable String id){

        courseService.publishCourseById(id);
        return R.ok();
    }
}
