package com.guli.edu.controller.admin;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.guli.common.vo.R;
import com.guli.edu.entity.Teacher;
import com.guli.edu.query.TeacherQuery;
import com.guli.edu.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/edu/teacher")
public class TeacherAdminController {
    @Autowired
    private TeacherService teacherService;

    /**
     * 查询所有讲师
     */
    @GetMapping
    public R list() {
        return R.ok().data("items", teacherService.list(null));
    }

    /**
     * 逻辑删除
     */
    @DeleteMapping("{id}")
    public R removeById(@PathVariable("id") String id) {
        teacherService.removeById(id);
        return R.ok();
    }

    /**
     * 分页查询
     */
    @GetMapping("{page}/{limit}")
    public R pageList(@PathVariable Long page, @PathVariable Long limit, TeacherQuery teacherQuery) {
        Page<Teacher> pageParam = new Page<>(page, limit);
        teacherService.pageQuery(pageParam, teacherQuery);
        List<Teacher> records = pageParam.getRecords();
        long total = pageParam.getTotal();
        return R.ok().data("total", total).data("rows", records);
    }

    /**
     * 新增
     */
    @PostMapping
    public R save(@RequestBody Teacher teacher) {
        teacherService.save(teacher);
        return R.ok();
    }

    /**
     * 修改
     */
    @PutMapping("{id}")
    public R updateById(@PathVariable String id, @RequestBody Teacher teacher) {
        teacher.setId(id);
        teacherService.updateById(teacher);
        return R.ok();
    }

    /**
     * 根据id查询
     */
    @GetMapping("{id}")
    public R getById(@PathVariable String id) {
        Teacher teacher = teacherService.getById(id);
        return R.ok().data("item", teacher);
    }
}