package com.guli.edu.vo;

import com.guli.edu.entity.Subject;
import lombok.Data;

import java.util.List;

/**
 * @author jl
 * @since 2019-12-12 10:18
 */
@Data
public class SubjectNode extends Subject {

    private List<SubjectNode> children;
}
