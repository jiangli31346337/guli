package com.guli.edu.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 视频信息
 */
@Data
public class VideoVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;
    private String title;
    private Boolean free;
    private String videoSourceId;
}