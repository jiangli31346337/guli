package com.guli.edu.vo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 课程信息,网站课程详情页需要的相关字段
 */
@Data
public class CourseWebVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;


	/**
	 * 课程标题
	 */
	private String title;

	/**
	 * 课程销售价格，设置为0则可免费观看
	 */
	private BigDecimal price;

	/**
	 * 总课时
	 */
	private Integer lessonNum;

	/**
	 * 课程封面图片路径
	 */
	private String cover;

	/**
	 * 销售数量
	 */
	private Long buyCount;

	/**
	 * 浏览数量
	 */
	private Long viewCount;

	/**
	 * 课程简介
	 */
	private String description;

	/**
	 * 讲师ID
	 */
	private String teacherId;

	/**
	 * 讲师姓名
	 */
	private String teacherName;

	/**
	 * 讲师资历,一句话说明讲师
	 */
	private String intro;

	/**
	 * 讲师头像
	 */
	private String avatar;

	/**
	 * 课程类别ID
	 */
	private String subjectLevelOneId;

	/**
	 * 类别名称
	 */
	private String subjectLevelOne;

	/**
	 * 课程类别ID
	 */
	private String subjectLevelTwoId;

	/**
	 * 类别名称
	 */
	private String subjectLevelTwo;
	
}