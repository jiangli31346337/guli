package com.guli.edu.mapper;

import com.guli.edu.entity.Course;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guli.edu.vo.CoursePublishVo;
import com.guli.edu.vo.CourseWebVo;

/**
 * <p>
 * 课程 Mapper 接口
 * </p>
 *
 * @author jiangli
 * @since 2019-11-28
 */
public interface CourseMapper extends BaseMapper<Course> {

	CoursePublishVo selectCoursePublishVoById(String id);

	CourseWebVo selectCourseWebVoById(String courseId);

}
