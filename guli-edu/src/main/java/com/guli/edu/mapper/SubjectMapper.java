package com.guli.edu.mapper;

import com.guli.edu.entity.Subject;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.guli.edu.vo.SubjectNode;

import java.util.List;

/**
 * <p>
 * 课程科目 Mapper 接口
 * </p>
 *
 * @author jiangli
 * @since 2019-11-28
 */
public interface SubjectMapper extends BaseMapper<Subject> {

    List<SubjectNode> selectNestedListByParentId(String parentId);

}
