package com.guli.edu.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.guli.edu.entity.Subject;
import com.guli.edu.vo.SubjectNode;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * <p>
 * 课程科目 服务类
 * </p>
 *
 * @author jiangli
 * @since 2019-11-28
 */
public interface SubjectService extends IService<Subject> {

    List<String> batchImport(MultipartFile file) throws Exception;

    List<SubjectNode> subjectList();

    List<SubjectNode> subjectList2();

    Boolean deleteSubjectById(String id);

    boolean saveLevel(Subject subject);

}
