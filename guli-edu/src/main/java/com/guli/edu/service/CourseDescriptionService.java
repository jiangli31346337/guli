package com.guli.edu.service;

import com.guli.edu.entity.CourseDescription;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程简介 服务类
 * </p>
 *
 * @author jiangli
 * @since 2019-11-28
 */
public interface CourseDescriptionService extends IService<CourseDescription> {

}
