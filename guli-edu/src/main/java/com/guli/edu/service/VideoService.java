package com.guli.edu.service;

import com.guli.edu.dto.VideoInfoForm;
import com.guli.edu.entity.Video;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 课程视频 服务类
 * </p>
 *
 * @author jiangli
 * @since 2019-11-28
 */
public interface VideoService extends IService<Video> {

	void saveVideoInfo(VideoInfoForm videoInfoForm);

	VideoInfoForm getVideoInfoFormById(String id);

	void updateVideoInfoById(VideoInfoForm videoInfoForm);

	void removeVideoById(String id);
}
