package com.guli.edu.service;

import com.guli.edu.entity.Chapter;
import com.baomidou.mybatisplus.extension.service.IService;
import com.guli.edu.vo.ChapterVo;

import java.util.List;

/**
 * <p>
 * 课程 服务类
 * </p>
 *
 * @author jiangli
 * @since 2019-11-28
 */
public interface ChapterService extends IService<Chapter> {

	void removeChapterById(String id);

	List<ChapterVo> nestedList(String courseId);
}
