package com.guli.edu.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

@Data
public class CourseInfoForm implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 课程ID
	 */
	private String id;

	/**
	 * 课程讲师ID
	 */
	private String teacherId;

	/**
	 * 课程专业ID
	 */
	private String subjectId;

	/**
	 * 课程专业父级ID
	 */
	private String subjectParentId;

	/**
	 * 课程标题
	 */
	private String title;

	/**
	 * 课程销售价格，设置为0则可免费观看
	 */
	private BigDecimal price;

	/**
	 * 总课时
	 */
	private Integer lessonNum;

	/**
	 * 课程封面图片路径
	 */
	private String cover;

	/**
	 * 课程简介
	 */
	private String description;
}