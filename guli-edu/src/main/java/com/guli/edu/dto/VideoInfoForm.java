package com.guli.edu.dto;

import lombok.Data;

/**
 * @author jl
 * @since 2019-12-16 15:42
 * 课时基本信息,编辑课时基本信息的表单对象
 */
@Data
public class VideoInfoForm {

	/**
	 * 视频ID
	 */
	private String id;

	/**
	 * 节点名称
	 */
	private String title;

	/**
	 * 课程ID
	 */
	private String courseId;

	/**
	 * 章节ID
	 */
	private String chapterId;

	/**
	 * 视频资源
	 */
	private String videoSourceId;

	/**
	 * 视频文件名称
	 */
	private String videoOriginalName;

	/**
	 * 显示排序
	 */
	private Integer sort;

	/**
	 * 是否可以试听：0默认 1免费
	 */
	private Boolean free;

}