package com.guli.oss.controller;

import com.guli.common.vo.R;
import com.guli.oss.service.FileService;
import com.guli.oss.util.ConstantPropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author jl
 * @description
 * @date 2019-12-06 16:16
 */
@RestController
@CrossOrigin
@RequestMapping("/admin/oss/file")
public class FileController {
    @Autowired
    private FileService fileService;

    /**
     * 文件上传
     */
    @PostMapping("upload")
    public R upload(@RequestParam("file") MultipartFile file, @RequestParam(value = "host", required = false) String host) {
        // 如果传了host,就存储到指定文件夹目录下
        if (!StringUtils.isEmpty(host)) {
            ConstantPropertiesUtil.FILE_HOST = host;
        }
        String uploadUrl = fileService.upload(file);
        return R.ok().message("文件上传成功").data("url", uploadUrl);

    }

}
