package com.guli.oss;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author jl
 * @description
 * @date 2019-12-06 15:05
 */
@SpringBootApplication
public class OSSApplication {

    public static void main(String[] args) {
        SpringApplication.run(OSSApplication.class,args);
    }
}
