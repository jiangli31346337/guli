package com.guli.sysuser.controller;

import com.guli.common.vo.R;
import com.guli.sysuser.entity.Sysuser;
import org.springframework.web.bind.annotation.*;

@CrossOrigin //跨域
@RestController
@RequestMapping("/admin/sysuser")
public class SysUserController {

    //用户登录接口
    @PostMapping("login")
    public R login(@RequestBody Sysuser sysuser){
        return R.ok().data("token", "admin");
    }

    //获取用户信息
    @GetMapping("info")
    public R info(@RequestParam String token){
        return R.ok()
                .data("roles", "admin")
                .data("name", "admin")
                .data("avatar","https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
    }

    //用户退出
    @PostMapping("logout")
    public R logout(){
        return R.ok();
    }

}