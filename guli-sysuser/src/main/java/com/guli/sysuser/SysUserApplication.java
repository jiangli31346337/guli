package com.guli.sysuser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * @author jl
 * @description
 * @date 2019-12-04 13:51
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SysUserApplication {
    public static void main(String[] args) {
        SpringApplication.run(SysUserApplication.class,args);
    }
}
