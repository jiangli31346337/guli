package com.guli.statistics;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableFeignClients
@ComponentScan(basePackages={"com.guli.statistics","com.guli.common"})
public class StatisticsApplication {
	public static void main(String[] args) {
		SpringApplication.run(StatisticsApplication.class, args);
	}
}