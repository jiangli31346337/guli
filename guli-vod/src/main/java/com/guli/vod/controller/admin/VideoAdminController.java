package com.guli.vod.controller.admin;

import com.aliyuncs.vod.model.v20170321.CreateUploadVideoResponse;
import com.aliyuncs.vod.model.v20170321.RefreshUploadVideoResponse;
import com.guli.common.vo.R;
import com.guli.vod.service.VideoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * 阿里云视频点播微服务
 */
@CrossOrigin
@RestController
@RequestMapping("/admin/vod/video")
public class VideoAdminController {

	@Autowired
	private VideoService videoService;

	@PostMapping("/upload")
	public R uploadVideo(@RequestParam("file") MultipartFile file) {

		String videoId = videoService.uploadVideo(file);
		return R.ok().message("视频上传成功").data("videoId", videoId);
	}

	@DeleteMapping("{videoId}")
	public R removeVideo(@PathVariable String videoId){
		videoService.removeVideo(videoId);
		return R.ok().message("视频删除成功");
	}

	/**
	 * 获取视频上传地址和凭证
	 * @param title 视频标题
	 * @param fileName 视频源文件名
	 */
	@GetMapping("get-upload-auth-and-address/{title}/{fileName}")
	public R getUploadAuthAndAddress(@PathVariable String title, @PathVariable String fileName){
		CreateUploadVideoResponse response = videoService.getUploadAuthAndAddress(title, fileName);
		return R.ok().message("获取视频上传地址和凭证成功").data("response", response);
	}

	/**
	 * 刷新视频上传地址和凭证
	 * @param videoId 云端视频id
	 */
	@GetMapping("refresh-upload-auth-and-address/{videoId}")
	public R refreshUploadAuthAndAddress(@PathVariable String videoId){
		RefreshUploadVideoResponse response = videoService.refreshUploadAuthAndAddress(videoId);
		return R.ok().message("刷新视频上传地址和凭证成功").data("response", response);
	}
}