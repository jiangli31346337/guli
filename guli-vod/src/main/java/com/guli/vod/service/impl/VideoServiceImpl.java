package com.guli.vod.service.impl;

import com.aliyun.vod.upload.impl.UploadVideoImpl;
import com.aliyun.vod.upload.req.UploadStreamRequest;
import com.aliyun.vod.upload.resp.UploadStreamResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.vod.model.v20170321.*;
import com.guli.common.constants.ResultCodeEnum;
import com.guli.common.exception.RRException;
import com.guli.vod.service.VideoService;
import com.guli.vod.utlis.AliyunVodSDKUtils;
import com.guli.vod.utlis.ConstantPropertiesUtil;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author jiangli
 * @since 2019/12/27 9:33
 */
@Service
public class VideoServiceImpl implements VideoService {

	/**
	 * 使用阿里云java sdk上传视频到oss
	 * @return 文件id
	 */
	@Override
	public String uploadVideo(MultipartFile file) {
		String videoId = null;
		try{
			InputStream inputStream = file.getInputStream();
			String fileName = file.getOriginalFilename();
			String title = fileName.substring(0, fileName.lastIndexOf("."));

			//创建请求对象
			UploadStreamRequest request = new UploadStreamRequest(
					ConstantPropertiesUtil.ACCESS_KEY_ID,
					ConstantPropertiesUtil.ACCESS_KEY_SECRET,
					title,
					fileName,
					inputStream);

			//创建文件上传器
			UploadVideoImpl uploader = new UploadVideoImpl();

			//执行文件上传，得到响应对象
			UploadStreamResponse response = uploader.uploadStream(request);

			//得到视频的videoId
			videoId = response.getVideoId();

		}catch (IOException e){
			throw new RRException(ResultCodeEnum.VIDEO_UPLOAD_ERROR);
		}

		return videoId;
	}

	@Override
	public void removeVideo(String videoId) {

		DefaultAcsClient client = null;
		try {
			client = AliyunVodSDKUtils.initVodClient(
					ConstantPropertiesUtil.ACCESS_KEY_ID,
					ConstantPropertiesUtil.ACCESS_KEY_SECRET);

			DeleteVideoRequest request = new DeleteVideoRequest();
			request.setVideoIds(videoId);

			client.getAcsResponse(request);

		} catch (ClientException e) {
			throw new RRException(ResultCodeEnum.VIDEO_DELETE_ALIYUN_ERROR);
		}

	}

	@Override
	public CreateUploadVideoResponse getUploadAuthAndAddress(String title, String fileName) {
		try {
			//初始化
			DefaultAcsClient client = AliyunVodSDKUtils.initVodClient(
					ConstantPropertiesUtil.ACCESS_KEY_ID,
					ConstantPropertiesUtil.ACCESS_KEY_SECRET);

			//创建请求对象
			CreateUploadVideoRequest request = new CreateUploadVideoRequest();
			request.setTitle(title);
			request.setFileName(fileName);

			//获取响应
			return client.getAcsResponse(request);

		} catch (ClientException e) {
			throw new RRException(ResultCodeEnum.FETCH_VIDEO_UPLOADAUTH_ERROR);
		}
	}

	@Override
	public RefreshUploadVideoResponse refreshUploadAuthAndAddress(String videoId) {
		try {
			//初始化
			DefaultAcsClient client = AliyunVodSDKUtils.initVodClient(
					ConstantPropertiesUtil.ACCESS_KEY_ID,
					ConstantPropertiesUtil.ACCESS_KEY_SECRET);

			//创建请求对象
			RefreshUploadVideoRequest request = new RefreshUploadVideoRequest();
			request.setVideoId(videoId);

			//获取响应
			return client.getAcsResponse(request);

		} catch (ClientException e) {
			throw new RRException(ResultCodeEnum.REFRESH_VIDEO_UPLOADAUTH_ERROR);
		}
	}

	@Override
	public String getVideoPlayAuth(String videoId) {
		DefaultAcsClient client = null;
		try {
			client = AliyunVodSDKUtils.initVodClient(
					ConstantPropertiesUtil.ACCESS_KEY_ID,
					ConstantPropertiesUtil.ACCESS_KEY_SECRET);

			//请求
			GetVideoPlayAuthRequest request = new GetVideoPlayAuthRequest();
			request.setVideoId(videoId);

			//响应
			GetVideoPlayAuthResponse response = client.getAcsResponse(request);

			//得到播放凭证
			String playAuth = response.getPlayAuth();

			return playAuth;
		} catch (ClientException e) {
			throw new RRException(ResultCodeEnum.FETCH_VIDEO_PLAYAUTH_ERROR);
		}
	}
}
