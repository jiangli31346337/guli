package com.guli.vod.service;

import com.aliyuncs.vod.model.v20170321.CreateUploadVideoResponse;
import com.aliyuncs.vod.model.v20170321.RefreshUploadVideoResponse;
import org.springframework.web.multipart.MultipartFile;

public interface VideoService {

	/**
	 * 上传视频
	 */
	String uploadVideo(MultipartFile file);

	/**
	 * 删除视频
	 */
	void removeVideo(String videoId);

	/**
	 * 获取上传凭证和上传地址
	 */
	CreateUploadVideoResponse getUploadAuthAndAddress(String title, String fileName);

	/**
	 * 刷新上传凭证
	 */
	RefreshUploadVideoResponse refreshUploadAuthAndAddress(String videoId);

	String getVideoPlayAuth(String videoId);
}